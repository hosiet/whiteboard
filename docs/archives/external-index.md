---
tags:
  - qBittorrent
  - 站外文档
---

# 外站链接

一些有用的外部站点。

## 网站

- [Creative Commons](https://creativecommons.org)  
    知识共享运动
- [PCGameWiki](https://www.pcgamingwiki.com/wiki/Home)  
    游戏概览信息 wiki，可用于查询分辨率、HDR 支持和 DRM 保护信息等内容。
- [初之图库](https://img.himiku.com/)  
    该图库保存了琉璃神社的每月壁纸包。
- [XIU2/TrackersListCollection](https://trackerslist.com/#/zh)  
    每天更新！全网热门 BitTorrent Tracker 列表！  
    同时包含了对于 tracker 和 BT 的介绍。
- [中文博客聚合订阅](https://box.othing.xyz/i/)  
    由 [Save The Web](https://github.com/saveweb) 搭建的中文独立博客 RSS 订阅。
- [中国科技大学测速网站](http://test.ustc.edu.cn/)
- [FreeDict Forum](https://forum.freemdict.com/)  
    词典爱好者之家
- [Alternative To](https://alternativeto.net/)  
    查询你使用的软件的流行替代品
- [LineageOS 官方设备维护列表](https://wiki.lineageos.org/devices/)
- [Hack News](https://news.ycombinator.com/)  
    各种科技新闻

## 个人博客

- [依云's Blog](http://blog.lilydjwg.me/)
- [Farseerfc 的小窩](https://farseerfc.me/)
- [Houge's Madness Blog](https://litterhougelangley.club/blog/)

## 实用文档

|名称|介绍|
|---|---|
|[comwrg/package-manager-proxy-settings](https://github.com/comwrg/package-manager-proxy-settings)|记录各个包管理器代理设置坑点。|
|[eryajf-world/Thanks-Mirror](https://github.com/eryajf-world/Thanks-Mirror)|整理记录各个包管理器，系统镜像，以及常用软件的好用镜像。|
|[archlinux 简明指南](https://arch.icekylin.online/)|📖 让 archlinux 成为你的常用系统吧！ ❤️️|
|[Fedora Docs](https://docs.fedoraproject.org/en-US/docs/)|Fedora Linux 用户文档|
|[openSUSE Docs](https://doc.opensuse.org/)|openSUSE Linux 用户文档|