---
hide:
  - tags
---

# 欢迎来到我的个人文档站！

![cover](./assets/twilight.jpg)

> [图片]由 [Rodney Campbell] 基于 [cc by-nc-nd 2.0] 授权使用。

[图片]: https://www.flickr.com/photos/rodneycampbell/22106869435/in/photostream/
[Rodney Campbell]: https://www.flickr.com/photos/rodneycampbell/
[cc by-nc-nd 2.0]: https://creativecommons.org/licenses/by-nc-nd/2.0/

## 关于本站

=== "本月更新"

    - [LibreOffice 项目和社区月度总结：2023 年 8 月](./translation/tdf-2023-08-recap.md)
    - [文档基金会发布 LibreOffice 7.5.6 Community](./translation/tdf-libreoffice7.5.6-released.md)
    - [公开信： 使用生成式人工智能技术的艺术家要求美国国会给予席位](./translation/open-letter-cc-gai.md)
    - [LibreOffice 7.6 的下载量达到 150 万次！](./translation/tdf-1.5million.md)
    - [Fedora Linux 39 Beta 版本现已发布](./translation/fedora39-beta-released.md)
    - [让我们来谈谈 Wayland 这件事吧](./translation/kde-wayland.md)
    - [LibreOffice 与谷歌编程之夏 2023：活动成果](./translation/tdf-gsoc-2023.md)

=== "主要内容"

    - 开源组织的新闻或博客的译文
    - 一些说明文档，备忘录
    - 个人随笔杂谈
    - 有趣的东西……

=== "支持的功能"

    - 站内搜索 ✅
    - 日夜间主题切换 ✅
    - [标签地图](./about/tags.md) ✅
    - [RSS 订阅](./blog/posts/hello-world.md) ✅

=== "不支持的功能"

    - 评论系统 ❌
    - M17N ❌

=== "This Site Powered By"

    ![cc](./assets/logo/CC-logo.svg){ width=20% }　
    ![cloudflare](./assets/logo/Cloudflare_Logo.svg){ width=20% }　
    ![gitlab](./assets/logo/GitLab_logo.svg){ width=20% }　
    ![python](./assets/logo/python-logo-generic.svg){ width=20% }　

欢迎提交 [PR] 或 [issue] 以协助改进站点内容。许可证清单详见[此处]。<br />Feel free to submit [PR] or [issue] to help improve site content. See [here][此处] for a list of licenses.

[PR]: https://gitlab.com/reuleaux-triangle/whiteboard/-/merge_requests
[issue]: https://gitlab.com/reuleaux-triangle/whiteboard/-/issues
[此处]: ./about/license.md