---
tags:
  - 杂谈
  - Z-Library
---

# 访问 Z-Library 的方法

Z-Library 是世界上最大的数字图书馆之一，本文记录如何访问该网站。

## 网站链接

- 公网：<https://zlibrary-global.se> 或 <https://singlelogin.re>
- Tor：<http://loginzlib2vrak5zzpcocc3ouizykn6k5qecgj2tzlnab5wcbqhembyd.onion>
- I2P：<http://zlib24th6ptyb4ibzn3tj2cndqafs6rhm4ed4gruxztaaco35lka.b32.i2p>

如果上述链接无法访问，你可以前往它的[维基词条][wiki]或 [Reddit] 查询可用的最新链接。

[wiki]: https://en.wikipedia.org/wiki/Z-Library
[Reddit]: https://www.reddit.com/r/zlibrary

## 电子邮箱

你可以通过向 [blackbox@zlib.se] 发送电子邮件，来创建 Z-Library 账户，修改密码或恢复已有的 Z-Library 账户。

[blackbox@zlib.se]: mailto:blackbox@zlib.se

## App

你可以通过 Z-Library 的 Android 应用程序直接访问 Z-Library，下载图书。App 可通过 Z-Library 官网的 `/z-access#android_app_tab` 子页面下载获得，登陆 App 需要输入或扫描此页面列出的 Token 或二维码。

## Telegram 机器人

Telegram 机器人另一个值得考虑的便捷访问方式。

你需要前往官网的 `/z-access#telegram_bot_tab` 子页面。

1. 在 Telegram 上搜索并找到 `@BotFather`，它是用户用于控制机器人的总控制机器人；
2. 向 `@BotFather` 发送 `/newbot` 命令，依照指示，新建一个机器人；
3. 点击 `/z-access#telegram_bot_tab` 子页面的 **现在就试试！** 按钮，滑到页面底部；
4. 将 `@BotFather` 最后一条消息完整地并将其粘贴到对话框之中。
5. 点击连接。

链接成功后，你可以向机器人发送关键字，获取图书下载列表。

### Telegram 频道

Z-Library 的官方 Telegram 频道地址是 `@zlibrary_official`。

## 独享域名

> 无需使用TOR、I2P暗网，现在您只需通过明网（公开互联网）的三级域名来访问Z-Library。我们的明网二级域名太容易被封了。相信使用独享域名，我们可以长久的提供图书馆服务。
>
> 请注意，独享域名，“独享”即可，不要外传！必须通过你自己的密码来访问独享域名，别人都用不了。

独享域名在 `/z-access#personal_domain_tab` 子页面可以找到。

----

## 其他

受美国邮政总局的 DMCA 诉讼影响，Z-Library 先前许多域名已被没收。同时，网络上也充斥着虚假的钓鱼网站。所以，请关注 Z-Library 的域名变更信息。