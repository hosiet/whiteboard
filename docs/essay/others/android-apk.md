---
tags:
  - 杂谈
  - Android
---

# Android 实用应用程序清单

!!! note "注意"

    以下清单基于 [LineageOS](https://lineageos.org) 20 进行举例。

- [Droid-ify](https://github.com/Iamlooker/Droid-ify): 使用 [Material UI](https://mui.com/material-ui/) 的 [F-Droid](https://f-droid.org/) 第三方客户端。
- [Aurora Store](https://f-droid.org/en/packages/com.aurora.store/)：匿名的 Google play 商店第三方客户端。
- [Simple Mobile Tools](https://www.simplemobiletools.com/)：开源的，简洁的基础软件套件。
    - [Simple Gallery Pro](https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/)：图库
    - [Simple SMS Messenger](https://f-droid.org/en/packages/com.simplemobiletools.smsmessenger)：短信
    - [Simple Notes Pro](https://f-droid.org/en/packages/com.simplemobiletools.notes.pro/)：文字笔记、备忘清单。
    - [Simple Contacts Pro](https://f-droid.org/en/packages/com.simplemobiletools.contacts.pro/)：联系人
- [K-9 Mail](https://f-droid.org/en/packages/com.fsck.k9/)：邮件
- [VLC](https://f-droid.org/en/packages/org.videolan.vlc/)：视频播放器
- [Tachiyomi](https://tachiyomi.org/)：漫画下载、阅读、订阅与管理软件
- [element.io](https://f-droid.org/en/packages/im.vector.app)：[matrix](https://matrix.org/) 客户端
- [KeepassDX](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre/)：密码管理器
- [Telegram](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=zh-CN)：IM 客户端
- [App Manager](https://f-droid.org/en/packages/io.github.muntashirakon.AppManager)：全功能的 Android 软件包管理器
- [Compass](https://f-droid.org/en/packages/com.bobek.compass)：指南针
- [LawnChair](https://lawnchair.app/)：类原生桌面
- [Magisk](https://github.com/topjohnwu/Magisk)：root 管理器
- [几何天气](https://f-droid.org/en/packages/wangdaye.com.geometricweather/)：天气预报
- [Material Files](https://f-droid.org/en/packages/me.zhanghai.android.files/)：文件管理器
- 代理软件：
    - [Clash For Android](https://github.com/Kr328/ClashForAndroid)
    - [Surfboard](https://github.com/getsurfboard/surfboard)
- [Poweramp](https://powerampapp.com/download-poweramp/)：音乐播放器
- [DictTango](https://forum.freemdict.com/t/topic/2354)：词典
- [Gboard](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin&hl=zh-CN)：输入法
- [雹](https://f-droid.org/en/packages/com.aistra.hail/)：应用冻结
- [Shizuku](https://apt.izzysoft.de/fdroid/index/apk/moe.shizuku.privileged.api): Shizuku 权限管理器
- [谷歌计算器](https://play.google.com/store/apps/details?id=com.google.android.calculator&hl=zh-CN)：简单计算器
- [图形计算器 Mathlab](https://play.google.com/store/apps/details?id=us.mathlab.android.calc.edu)：科学计算器、图形计算器
- [Office 365](https://play.google.com/store/apps/details?id=com.microsoft.office.officehubrow&hl=zh-CN)：办公文档查看器
- [Pure 轻雨](https://www.coolapk.com/apk/me.morirain.dev.iconpack.pure)：应用图标包
- [一叶日历](https://www.coolapk.com/apk/me.mapleaf.calendar)：日历
- [Scene](https://www.coolapk.com/apk/com.omarea.vtools)：系统调试工具
- [Cutoolbox](https://github.com/chenzyyzd/CuprumTurbo-Scheduler)：CPU 调度控制
- [PixShaft](https://play.google.com/store/apps/details?id=ceui.lisa.pixiv)：[Pixiv](https://www.pixiv.net/) 第三方客户端
- [Snapseed](https://play.google.com/store/apps/details?id=com.niksoftware.snapseed&hl=zh-CN)：照片编辑工具
- [Via 浏览器](https://play.google.com/store/apps/details?id=mark.via.gp&hl=zh-CN)：网络浏览器
- [Lithium](https://play.google.com/store/apps/details?id=com.faultexception.reader&hl=zh-CN)：电子书阅读器
- [ZArchiver](https://play.google.com/store/apps/details?id=ru.zdevs.zarchiver)：文件解压缩工具