---
tags:
  - 杂谈
  - Linux
  - openSUSE
---

# Tumbleweed 配置小记（一）

## 准备

[下载 ISO 文件]，例如：

[下载 ISO 文件]: ./../guide/misc/mirror-site.md

```
wget https://opentuna.cn/opensuse/tumbleweed/iso/openSUSE-Tumbleweed-DVD-x86_64-Current.iso
```

烧录工具可以使用 [Rufus] 或 [Fedora Media Writer]。

[Rufus]: https://rufus.ie
[Fedora Media Writer]: https://www.fedoraproject.org/en/workstation/download/

## 安装系统

启动安装介质后，不必联网

|分区|大小|格式|备注|
|---|---|---|---|
|/boot/efi|512MB|efi|挂载，格式化|
|SWAP|8GB|swap|挂载，格式化|
|/|50GB|btrfs|格式化，并勾选启用系统快照。|
|/home|-|xfs|仅挂载，手动清理文件|
|/bt|-|xfs|仅挂载，bt 做种文件存储分区|

- 需要删除的软件包：`ibus`、`fcitx`、`opensuse-welcome`、`kompare`、`discover`、`PackageKit`、`konversation`、`kmousetool`、`vlc`、`skanlite`
- 需要禁用的模组：`pattern:games`、`pattern:kde_pim`
- 需要安装的软件包：`git-core`、`flatpak`、`kleopatra`

**重装时不必导入旧用户数据或新建普通账户。**

## 初次启动

### 修改软件源

重装系统时，建议使用 `root` 身份登陆系统，如果是全新安装则无必要。

禁用全部的软件源：

```
zypper mr -da
```

添加第三方软件源并更新系统（可选），例如：

```
zypper ar -fcg https://opentuna.cn/opensuse/tumbleweed/repo/oss/ opentuna-oss
```
```
zypper ar -fcg https://opentuna.cn/opensuse/tumbleweed/repo/non-oss/ opentuna-non-oss
```
```
zypper ref && zypper dup -y
```

### 安装多媒体解码器

```
zypper ar -cfp 90 https://mirrors.aliyun.com/packman/suse/openSUSE_Tumbleweed/ packman
```
```
zypper refresh && zypper dist-upgrade --from packman --allow-vendor-change
```
```
zypper install --from packman ffmpeg gstreamer-plugins-{good,bad,ugly,libav} libavcodec-full
```

### 安装 proxychains-ng

设置 proxychains.conf：

```
zypper in proxychains-ng
```

```shell
nano /etc/proxychains.conf
-----
quiet_mode
socks5  127.0.0.1 7890
```

### 迁移旧数据

将旧用户目录重命名：

```
mv /home/poplar /home/poplar.old
```

新建用户：

```
useradd poplar
```
```
passwd poplar
```
```
usermod -aG wheel poplar
```

检查权限，并矫正权限：

```
ls -lat /home/poplar && id poplar
```
```
chown -R poplar:poplar /home/poplar.old
```

需要重新装入的应用程序数据：

```
/home/poplar.old/{公共,其他,文档,下载,图片,音乐,bin,game}
/home/poplar.old/.pip
/home/poplar.old/.vscode
/home/poplar.old/.bashrc
/home/poplar.old/.gitconfig
/home/poplar.old/.var
/home/poplar.old/.r

/home/poplar.old/.config/aria2
/home/poplar.old/.config/Code
/home/poplar.old/.config/deadbeef
/home/poplar.old/.config/fcitx5
/home/poplar.old/.config/goldendict
/home/poplar.old/.config/kate
/home/poplar.old/.config/keepassxc
/home/poplar.old/.config/mpv
/home/poplar.old/.config/pip
/home/poplar.old/.config/VirtualBox
/home/poplar.old/.config/qBittorrent
/home/poplar.old/.config/RSS Guard 4
/home/poplar.old/.config/crow-translate
/home/poplar.old/.config/clash-verge

/home/poplar.old/.local/share/fonts
/home/poplar.old/.local/share/fcitx5
/home/poplar.old/.local/share/konsole
/home/poplar.old/.local/share/qBittorrent
/home/poplar.old/.local/share/TelegramDesktop
/home/poplar.old/.local/share/Mindustry
```

## 以普通用户身份登陆系统

### 设置主机名

```
sudo hostnamectl set-hostname --pretty "White-Poplar's Laptop"
```
```
sudo hostnamectl set-hostname --static c004-h0
```

### 安装软件

|包名/名称|源|描述|子包/Flatpak 包名|
|---|---|---|---|
|`keepassxc`|发行版仓库|密码管理|
|`aria2c`|发行版仓库|下载工具|
|`mpv`|发行版仓库|多媒体播放器|
|`opi`|发行版仓库|OBS 仓库助手|
|`telegram-desktop`|发行版仓库|即时通讯软件|
|`gimp`|发行版仓库|图片编辑|
|`deadbeef`|发行版仓库|音乐播放器|
|`fcitx5`|发行版仓库|输入法|`fcitx5-configtool`、`fcitx5-chinese-addons`|
|`FreeFileSync`|发行版仓库|文件同步/比对|
|Czkawka|Flatpak Remote|文件查重工具|`com.github.qarmin.czkawka`|
|Calibre|Flatpak Remote|电子书阅读器|`org.freefilesync.FreeFileSync`|
|Draw.io|Flatpak Remote|思维导图工具|`com.jgraph.drawio.desktop`|
|Fedora Media Writer|Flatpak Remote|ISO 烧录工具|`org.fedoraproject.MediaWriter`|
|`virtualbox`|发行版仓库|虚拟机|
|`goldendict-ng`|发行版仓库|字典|`goldendict-ng-lang`|
|`code`|Microsoft|源代码编辑器|
|`crow-translate`|发行版仓库|翻译软件|`crow-translate`、`crow-translate-lang`|

```
sudo zypper in keepassxc mpv aria2c opi telegram-desktop gimp deadbeef fcitx5 fcitx5-configtool fcitx5-chinese-addons virtualbox goldendict-ng goldendict-ng-lang crow-translate crow-translate-lang FreeFileSync
```

修改用户组

```
sudo usermod -aG vboxusers $USER && sudo usermod -aG flatpak $USER
```

然后重启系统。

### 安装 VScode

```
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
```
```
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ntype=rpm-md\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/zypp/repos.d/vscode.repo'
```
```
sudo zypper ref && sudo zypper in code
```

此时，如果需要[美化 KDE](./eyecandy-kde.md)，建议此时开始，完成后再安装 Flatpak，以防止出现主题不匹配等问题。

### 安装相关的 Flatpak 软件包

添加仓库：

```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

安装软件：

```
flatpak install flathub com.calibre_ebook.calibre
```
```
flatpak install flathub com.jgraph.drawio.desktop
```
```
flatpak install flathub com.github.qarmin.czkawka
```
```
flatpak install flathub org.fedoraproject.MediaWriter
```
```
flatpak install flathub com.calibre_ebook.calibre
```

### 启动 tlp（可选）

```
sudo systemctl enable tlp --now
```
```
tlp-stat -s #依照提示，手动屏蔽相关内容
```
```
sudo systemctl status power-profiles-daemon.service tlp.service
```
```
sudo systemctl mask power-profiles-daemon.service
```

### NVIDIA 显卡驱动（可选）

```
sudo zypper ar --refresh https://download.nvidia.com/opensuse/tumbleweed NVIDIA && sudo zypper ref
```

```
sudo zypper in x11-video-nvidiaG06
```