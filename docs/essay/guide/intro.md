---
tags:
  - Linux 简明指南
---

# 关于

<center>

![](./caution.svg){ width=50% }

</center>

这是一份聚焦于 Linux 日常使用，记录 openSUSE/Fedora 等操作系统的安装、使用方法和细节信息；仅供站长个人使用、查阅、不完整，不官方，不权威的指南。