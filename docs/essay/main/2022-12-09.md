---
tags:
  - 随笔
  - Djot
  - 站点动态
  - YouTube
---

# 2022-12-08

## 火柴人 Vs Minecraft

啊啊啊啊啊，它居然更新了。

<iframe width="1519" height="553" src="https://www.youtube.com/embed/Sp2nxlrQ89w" title="The King - Animation vs. Minecraft Shorts Ep 30" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

这次算是一次阶段性的剧情收尾，非常精彩！

## Djot

最近抽空把 Djot 的文档翻译了一遍，估计要普及还需要一段时间。

- [Djot 用户手册](https://hanjingxue-boling.github.io/Djot_handbook_for_Zh-Hans/)

就个人感受而言吧，似乎还算不错，只不过目前依旧使用 markdown 为主。

## GFW

实测，我的文档站已经被 GFW 屏蔽了……