---
tags:
  - 随笔
  - 社区动态
  - Fedora
  - qBittorrent
---

# 2023-02-28

## 论坛合并

根据[新闻邮件](https://discussion.fedoraproject.org/t/downtime-this-friday-as-we-complete-the-merge-with-ask-fedora/47175/1)，askfedora 将于2023-03-03T17:00:00Z UTC 与 fedora discussion 合并。

## RSS proxy

根据 [GitHub](https://github.com/qbittorrent/qBittorrent/pull/18528)，qbittorrent 的下一个版本将会支持单独对 RSS 启用代理。

<center>

![01](./images/2023-02/rss.png)

</center>