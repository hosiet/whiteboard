---
tags:
  - 站务
---

# 关于我

<center>

![1](./images/one-last-neko-2.jpeg){ width=170px }

</center>

我是 [Poplar at twilight](https://gitlab.com/Poplar.at.twilight) (zh-Hans: `暮光的白杨`)，你可以简称我为**白杨**（en: `White Poplar`）。我是 [openSUSE Linux 中文维基](https://zh.opensuse.org/)和[社区新闻](https://suse.org.cn/)的翻译者之一。

- 关联账户：  
    - [Aui - openSUSE 中文论坛](https://forum.suse.org.cn/u/aui/summary)  
    - [White-poplar - openSUSE Wiki](https://zh.opensuse.org/User:White-poplar)  
    - [bh - AskFedora](https://discussion.fedoraproject.org/u/bh)  
- 曾用名：  
    - Hanjingxue Boling (*Hanjingxue* 或 *Boling H.*，zh-Hans: `寒晶雪•铂灵`)

## OpenPGP 公钥

你可以在[此处](./assets/White%20Poplar_0x001EFE63_public.asc)下载公钥。

PGP 指纹：`2B429BB9098D7B218ECBD4DAF8FAE5B1001EFE63`