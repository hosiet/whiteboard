---
tags:
  - 新闻
  - Blender
---

# Blender 项目将参加 GDC 2023

## 译文信息

- 原文：[Blender at GDC 2023](https://www.blender.org/events/blender-at-gdc-2023/)
- 作者：[Francesco Siddi](https://www.blender.org/author/fsiddi/)
- 许可证：[CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- 译者：暮光的白杨
- 日期：2023-03-17

----

Blender 有参加 [SIGGRAPH](https://www.youtube.com/watch?v=9G9I4WFe3B0) 的悠久传统（已经超过 20 年），在过去几年中，Blender 将更多的活动添加到日程表中，例如 CTN 和 [Annecy 动画电影节](https://www.annecyfestival.com/home)。

Blender 团队的一个小型代表团（Francesco Siddi 和 Dalai Felinto）今年也将参加在加利福尼亚州旧金山举行的[游戏开发者大会](https://gdconf.com/) (GDC)。主要目标是与合作伙伴建立联系并可用于行业外展。

如果您在 3 月 20 日至 24 日期间在湾区并希望联系我们，请访问 blender.org 联系 francesco。