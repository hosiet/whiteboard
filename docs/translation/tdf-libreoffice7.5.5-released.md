---
tags:
  - 新闻
  - LibreOffice
---

# LibreOffice 7.5.5 Community 现可供下载

- 译文信息：
    - 原文：[LibreOffice 7.5.5 Community available for download](https://blog.documentfoundation.org/blog/2023/07/20/libreoffice-7-5-5-community-available-for-download/)
    - 作者：[Mike Saunders](https://blog.documentfoundation.org/blog/author/mikesaunders/)
    - 许可证：[CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
    - 译者：暮光的白杨
    - 日期：2023-07-20

---

![banner](./images/2023-07/LO75_banner.png)

2023 年 7 月 20 日，柏林——LibreOffice 7.5.5 Community 是 LibreOffice 7.5 系列的第五个次版本，是一款由志愿者支持的桌面生产力自由办公套件。你可以从我们的[下载页面]获取适用于 Windows（Intel/AMD 和 ARM 处理器）、macOS（Apple Silicon 和 Intel 处理器）和 Linux 的安装程序[^1]。

[^1]: 变更日志页面：[RC1] 和 [RC2]

[RC1]: https://wiki.documentfoundation.org/Releases/7.5.5/RC1
[RC2]: https://wiki.documentfoundation.org/Releases/7.5.5/RC2
[下载页面]: https://www.libreoffice.org/download

鉴于 LibreOffice 7.6 即将发布，我们邀请所有用户更新到该版本，该版本已经过测试并受到广泛欢迎，可以用于生产环境。

基于与 LibreOffice 7.5.5 相同的 LibreOffice 技术的其他几种产品可用于主要的桌面操作系统（Windows、macOS、Chrome OS 和 Linux）、移动平台（Android 和 iOS）和云。在某些情况下，虽然基础技术相同，但这些产品名称并非是 LibreOffice。

对于企业级部署，TDF 强烈推荐生态系统合作伙伴提供的 [LibreOffice Enterprise 应用程序系列]（适用于桌面、移动和云），具有大量专用增值功能和 SLA（服务级别协议）等其他优势。

[LibreOffice Enterprise 应用程序系列]: https://www.libreoffice.org/download/libreoffice-in-business/

## LibreOffice 7.5.5 Community 的可用性

LibreOffice 7.5.5 Community 可从[本页][下载页面]获取。专有操作系统的最低要求为 Microsoft Windows 7 SP1 和 Apple macOS 10.14。[这里]列出了基于 LibreOffice 技术的 Android 和 iOS 产品。

文档基金会不为用户提供技术支持，但用户可以通过用户邮件列表和 [Ask LibreOffice] 网站上的志愿者获取帮助。

LibreOffice 用户、自由软件倡导者和社区成员可以在[此页面]上捐款来支持文档基金会。

[Ask LibreOffice]: https://ask.libreoffice.org/
[这里]: https://www.libreoffice.org/download/android-and-ios/
[此页面]: https://www.libreoffice.org/donate