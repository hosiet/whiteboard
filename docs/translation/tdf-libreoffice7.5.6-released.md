---
tags:
  - 新闻
  - LibreOffice
---

# 文档基金会发布 LibreOffice 7.5.6 Community

- 译文信息：
    - 原文：[The Document Foundation releases LibreOffice 7.5.6 Community](https://blog.documentfoundation.org/blog/2023/09/07/the-document-foundation-releases-libreoffice-7-5-6-community/)
    - 作者：[Mike Saunders](https://blog.documentfoundation.org/blog/author/mikesaunders/)
    - 许可证：[CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
    - 译者：暮光的白杨
    - 日期：2023-09-08

---

![banner](./images/2023-07/LO75_banner.png)

2023 年 9 月 7 日，柏林——LibreOffice 7.5.6 Community 是 LibreOffice 7.5 系列的第六个次版本，是由志愿者支持的用于提高桌面生产力的免费办公套件，可从我们的[下载页面]下载，适用于 Windows（Intel/AMD 和 ARM 处理器）、macOS（ Apple Silicon 和 Intel 处理器）和 Linux[^1]。

[^1]: 变更日志页面：[RC1] 和 [RC2]。

[RC1]: https://wiki.documentfoundation.org/Releases/7.5.6/RC1
[RC2]: https://wiki.documentfoundation.org/Releases/7.5.6/RC2
[下载页面]: https://www.libreoffice.org/download

基于 [LibreOffice 技术]的产品可用于主要桌面操作系统（Windows、macOS、Linux 和 Chrome OS）、移动平台（Android 和 iOS）和云。

[LibreOffice 技术]: https://www.libreoffice.org/discover/libreoffice-technology/

对于企业级部署，TDF（文档基金会）强烈推荐生态系统合作伙伴提供的 [LibreOffice Enterprise 系列应用程序]——适用于桌面、移动和云——具有大量专用增值功能和其他优势，如 SLA（服务水平协议）。

[LibreOffice Enterprise 系列应用程序]: https://www.libreoffice.org/download/libreoffice-in-business/

## LibreOffice 7.5.6 Community 的可用性

LibreOffice 7.5.6 Community 可从[此页面][下载页面]获取。专有操作系统的最低要求为 Microsoft Windows 7 SP1 和 Apple macOS 10.14。[这里]列出了基于 LibreOffice 技术的 Android 和 iOS 产品。

[这里]: https://www.libreoffice.org/download/android-and-ios/

文档基金会不为用户提供技术支持，但用户可以从用户邮件列表和 [Ask LibreOffice 网站]上的志愿者那里获得技术支持。

[Ask LibreOffice 网站]: https://ask.libreoffice.org/

LibreOffice 用户、自由软件倡导者和社区成员可以[通过捐款支持文档基金会]。

[通过捐款支持文档基金会]: https://www.libreoffice.org/donate