---
tags:
  - 新闻
  - Fedora
  - Anaconda
  - 社区动态
---

# 请对 Anaconda Web UI 存储功能进行反馈！

## 译文信息

- 源文：[Anaconda Web UI storage feedback requested!](https://fedoramagazine.org/anaconda-web-ui-storage-feedback-requested/)
- 作者：[Jiří Konečný](https://fedoramagazine.org/author/jkonecny/)
- 译者：暮光的白杨
- 许可证：[CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)
- 日期：2023-01-18

----

>![cover](./images/2023-01/anaconda-1-1024x433.jpg)
>*背景照片由 [David Cantrell](https://fedoraproject.org/wiki/User:Dcantrell) 拍摄（已裁剪）*

你可能知道 [Anaconda Web UI 预览镜像](https://fedoramagazine.org/anaconda-web-ui-preview-image-now-public)现在有一个简单的“擦除所有内容”分区，因为分区是一个相当大且难以对付的话题。一方面，Linux 大师们想要控制一切；另一方面，我们也需要支持新手用户。我们还受到现有后端和存储工具的能力以及与 Anaconda 其余部分的一致性的限制。Anaconda 团队向您寻求存储功能反馈，以帮助我们设计 Web UI！

一般来说，分区是 Anaconda 正在做的事情中最复杂、最难以对付和最有争议的部分之一。由于这一点以及[上一篇博客](./anaconda-webui-image-go-public.md)的反馈，我们决定再次征求您的反馈，以了解我们应该关注的重点。我们正在寻找每个人的反馈。答复越多越好。如果你正在使用 Fedora、RHEL、Debian、openSUSE、Windows 或 Linux，即使只是一个星期，我们也希望得到你的意见。所有这些投入都是有价值的!

请帮助我们塑造 Anaconda 安装程序中最复杂的部分之一！

只需花几分钟时间填写问卷，您就可以帮助我们决定选择哪条路径进行分区。

- 问卷链接：<https://redhatdg.co1.qualtrics.com/jfe/form/SV_87bPLycfp1ueko6>