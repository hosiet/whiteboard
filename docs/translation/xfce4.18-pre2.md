---
tags:
  - 新闻
  - xfce
  - 版权所有
---


# Xfce 4.18 Pre2（预览版）现已发布

## 译文信息

- 原文：[Xfce 4.18 Pre2 Released](https://alexxcons.github.io/blogpost_7.html)
- 作者：[Alexander Schwinn](https://alexxcons.github.io/index.html)
- 许可证：© 2022 Alexander Schwinn
- 译者：暮光的白杨
- 日期：2022-12-02

----

## Xfce 4.18 Pre2 现已发布

该平台预发布版（4.18 pre2）包含 Xfce 核心组件，Xfce 4.18 的最终版本很快将随后发布。我们修复了一些在 Xfce 4.18 pre1 中发现的问题，做了一些进一步的改进，然后现在将这些成果打包为 Xfce 4.18 pre2 进行发布。

在[计划于 12 月 15 日](https://wiki.xfce.org/releng/4.18/roadmap)发布的最终版本到来之前，我们邀请早期参与者尝试并检查（4.18 pre2 的）兼容性。

Xfce4.18 pre2 包括所有 Xfce 核心组件。用于构建它的压缩包可以在这里找到：

- [下载单独的压缩包](https://archive.xfce.org/xfce/4.18pre2/src)
- [下载一个包含全部内容的压缩包](https://archive.xfce.org/xfce/4.18pre2/fat_tarballs)

不过，你可能更愿意等待发行版特定的开发包。

## 壁纸比赛

我很高兴向您展示我们的 [Xfce4.18 壁纸比赛](https://gitlab.xfce.org/artwork/public/-/issues/1)的结果，该比赛收到了许多漂亮的壁纸提交。

本次比赛获胜的壁纸由 Katerina Shkel 制作，它将作为新的 Xfce 4.18 的默认壁纸： 

![winner](./images/2022-12/xfce-4.18-final.svg)

第二名和第三名均由 Denis Kuzminok 获得。这些壁纸也将在 Xfce 中发布：

![second](./images/2022-12/Xfce-4.18_blue_flower.svg)

![third](./images/2022-12/Xfce-4.18_leaf_mouse.svg)

如果你对比赛的完整竞品列表感兴趣，可以点击[此处](https://gitlab.xfce.org/artwork/public/-/issues/1)查看详情。

## 即将到来！

Xfce4.18 中所有新特性的详细总结计划在最终版本中发布，敬请期待！



致以诚挚的问候，

Xfce 开发团队