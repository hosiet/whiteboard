---
tags:
  - 新闻
  - 互联网档案馆
---

# 不要删除我们的书！举行集会

## 译文信息

- 作者：[chrisfreeland](https://blog.archive.org/author/chrisfreeland/)
- 源文：[Don’t Delete Our Books! Rally](https://blog.archive.org/2023/03/31/dont-delete-our-books-rally/)
- 许可证：不明
- 译者：暮光的白杨
- 日期：2023-04-02

<!-- <end of list> -->

- 译者注：  
    - 互联网档案馆在[初次诉讼][fight]中败诉了，但他们还会重新打官司。
    - 翻译本文的原因之一是，本站点也使用了互联网档案馆的 wayback machine 功能作为[网站快照][snapshot]。
    - 如果互联网档案馆真的败诉了，那商业出版社可以从这个非盈利组织抽走一大笔罚款，导致互联网档案直接陷入运营危机甚至倒闭。
    - 有关如何帮助互联网档案馆，详见[此文][help]。

[help]: http://blog.archive.org/2020/06/14/how-can-you-help-the-internet-archive/
[snapshot]: https://web.archive.org/web/20230000000000*/whiteboard-ui8.pages.dev/

---

## 正文

对于那些询问如何支持 [Internet Archive]（互联网档案馆）的人，我们将于 4 月 8 日星期六在互联网档案馆的台阶上举行集会，时间是上午 11 点（太平洋时间）。

[Internet Archive]: https://archive.org

<center><strong>
[了解更多并注册活动][rally]
</strong></center>

[rally]: https://actionnetwork.org/events/dont-delete-our-books-rally-in-san-francisco

> 转自 <https://actionnetwork.org/events/dont-delete-our-books-rally-in-san-francisco>
>
> **为图书馆的数字化未来集会！**
>
> 非营利性的互联网档案馆正在对一项威胁到所有图书馆的未来的[判决][fight]提出上诉。大型出版商正在起诉，以切断图书馆对数字书籍的所有权和控制权，为审查和监控开辟新的道路。如果这一裁决被允许成立，它将导致：
>
> - 审查加强，甚至仅由出版业大股东决定是否删除书籍；
> - 大型科技公司越来越多地侵入图书馆读者的数据，通过对读者阅读或研究内容的个人隐私信息进行货币化，侵害读者的利益；
> - 大型媒体垄断企业甚至会索求更高昂的授权费，他们正在吞噬公共和学校图书馆的预算；
> - 减少每个社区的人们获得书籍的机会；
> - 人们失去作为大量历史和文化保存者的图书馆，因为它们永远不会被允许拥有和保存数字图书。
>
> 如需更多信息，请访问 [BattleForLibraries.com][blc]。**该网站的组织者将于 2023 年 4 月 8 日星期六上午 11 点，在位于旧金山 Funston 街的互联网档案馆[举行集会][rally]。**
>
> 欢迎所有人的加入。请带上指示牌（我们也会分享一些指示牌！）并加入我们的行列，捍卫图书馆拥有和保存书籍的权利——无论是数字版还是印刷版。

[blc]: http://battleforlibraries.com/
[fight]: https://blog.archive.org/2023/03/25/the-fight-continues/