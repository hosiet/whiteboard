---
tags:
  - 新闻
  - Xfce
  - 壁纸
  - 版权所有
---

# Xfce 4.18 壁纸竞赛

## 作品信息

- 原文：[Wallpaper Contest for Xfce 4.18](https://alexxcons.github.io/blogpost_5.html)
- 作者：[Alexander Schwinn](https://gitlab.xfce.org/alexxcons)
- 许可证：未知
- 译者：暮光的白杨
- 日期：2022-10-21

----

可能您已经听说 [Xfce 4.18 计划于今年 12 月 15 日发布](https://wiki.xfce.org/releng/4.18/roadmap)。

如果该版本有一个新的默认壁纸就好了。这次的想法是请 Xfce 社区贡献几张壁纸；再通过社区投票，选出 Xfce 4.18 的默认壁纸。

因此，如果您具备所需的技能，我们将非常欢迎您的贡献！

和[旧的 Xfce 壁纸](https://gitlab.xfce.org/artwork/public/-/tree/master/wallpapers)一样，新的壁纸应该是矢量图形，上面有未修改的 [Xfce 鼠标](https://alexxcons.github.io/(https://gitlab.xfce.org/artwork/public/-/tree/master/logo))和一些花哨的形状。

请注意，提交的壁纸必须是你自己的原创作品，并且需要以 [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 进行授权。如果您未在帖子中指定许可，那么我们将假定您正在根据 CC BY-SA 4.0 许可壁纸。

图片不应在元信息中提及用于创建壁纸的（FOSS 或专有）软件。

提交截止日期为 11 月 20 日。

为了参与比赛，请将您的壁纸发布到[相应的 gitlab issue 中](https://gitlab.xfce.org/artwork/public/-/issues/1)！