---
tags:
  - 新闻
  - LibreOffice
---

# 文档基金会发布 LibreOffice 7.5.4 Community

## 译文信息

- 原文：[The Document Foundation releases LibreOffice 7.5.4 Community](https://blog.documentfoundation.org/blog/2023/06/08/tdf-releases-lo-7-5-4-community/)
- 作者：[Italo Vignoli](https://blog.documentfoundation.org/blog/author/italovignoli/)
- 许可证：[CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- 译者：暮光的白杨
- 日期：2023-06-09

----

![cover](./images/2023-06/LO75_banner-1024x201.png)

## 正文

2023 年 6 月 8 日，柏林：

LibreOffice 7.5.4 Community 是 LibreOffice 7.5 系列的第四个次要版本，是志愿者支持的自由桌面办公套件，你可从 <https://www.libreoffice.org/download> 下载适用于 Windows（Intel/AMD 和 ARM 处理器）、macOS（Apple Silicon 和 Intel 处理器）和 Linux 的安装包[^1]。

[^1]: 变更日志详见：  
  - <https://wiki.documentfoundation.org/Releases/7.5.4/RC1>  
  - <https://wiki.documentfoundation.org/Releases/7.5.4/RC2>

基于 LibreOffice 技术的产品可用于主要桌面操作系统（Windows、macOS、Linux 和 Chrome OS）、移动平台（Android 和 iOS）和云端。

对于企业级部署，TDF[^2] 强烈推荐来自生态系统合作伙伴的 [LibreOffice Enterprise] 系列应用程序——适用于桌面、移动和云端——具有大量专用增值功能和其他优势，例如 [SLA]（服务水平协议）。

[LibreOffice Enterprise]: https://www.libreoffice.org/download/libreoffice-in-business/
[SLA]: https://en.wikipedia.org/wiki/Service-level_agreement

[^2]: The Document Foundation，即文档基金会，简写为 TDF。

### LibreOffice 7.5.4 Community 的可用性

你可以从[此处][dl]下载获取 LibreOffice 7.5.4 Community。要安装 LibreOffice，对于专有操作系统的最低要求是 Microsoft Windows 7 SP1 和 Apple macOS 10.14。基于 LibreOffice 技术，适用于 Android 和 iOS 的产品列表详见[此处][android]。

[dl]: https://www.libreoffice.org/download/
[android]: https://www.libreoffice.org/download/android-and-ios/
[Ask LibreOffice]: https://ask.libreoffice.org
[donate]: https://www.libreoffice.org/donate

尽管用户可以从用户邮件列表和 [Ask LibreOffice] 网站上的志愿者那里获得技术支持，但文档基金会不为用户提供技术支持。

LibreOffice 用户、自由软件倡导者和社区成员可以通过[该网站][donate]捐款支持文档基金会。