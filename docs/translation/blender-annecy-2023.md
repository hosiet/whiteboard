---
tags:
  - 新闻
  - Blender
---

# Blender 2023 年安纳西电影节概要

## 译文信息

- 原文：[Blender at Annecy 2023 Recap](https://www.blender.org/events/blender-at-annecy-2023-recap/)
- 作者：[Pablo Vazquez](https://www.blender.org/author/venomgfx/)
- 许可证：[CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- 译者：暮光的白杨
- 日期：2023-06-30

----

几周前，Blender 团队的一个大型代表团参加了[安纳西电影节][annecy]。这个节日——以及 MIFA 电影市场——与充满活力的动画社区一起不断成长。以下是这次活动的一些印象：

[annecy]: https://www.annecyfestival.com/home

<iframe width="714" height="398" src="https://www.youtube.com/embed/RuLiEsIgHGo" title="Blender at Annecy 2023" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

电影节现场的 Blender 展台总是很繁忙，每天都有数百名与会者来打招呼，分享 Blender 对他们的个人或职业生活的帮助。Blender 确实正在产生影响，尤其是在新兴市场，第一批“完全本土化”的大型动画电影项目正在形成。此外，一些学校也首次开始注意到行业对 Blender 的缓慢转变，并计划在其课程中引入 Blender。

除了电影市场之外，Blender 早餐还在老城区的一家咖啡馆举行。超过 70 名与会者（一些知名的社区成员和一些新人）参与其中，这是一个分享 Blender 项目更新、建立网络和联系的机会。

明年见！