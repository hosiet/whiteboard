---
tags:
  - 新闻
  - Blender
---

# 纪录片系列《Blenderheads》

## 译文信息

- 源文：[Blenderheads: A Documentary Series](https://www.blender.org/news/blenderheads-a-documentary-series/)
- 作者：[Francesco Siddi](https://www.blender.org/author/fsiddi/)
- 许可证：[CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- 译者：暮光的白杨
- 日期：2023-01-25

---

今天，Blender 基金会发布了《[Blenderheads](https://www.youtube.com/watch?v=V_dZtidLwo4)》的第一集，这是一个关于 Blender 项目背后的人的系列。编辑兼导演——纪录片制作人 Maaike Kleverlaan——在 Blender 总部工作，负责报道活动和进行采访。第一集定于 2022 年 9 月至 12 月，每季度将发布新剧集。

<iframe width="710" height="394" src="https://www.youtube.com/embed/V_dZtidLwo4" title="BLENDERHEADS - Ep. 1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Blenderheads 跟随参与 Blender 项目的人们的旅程，记录创建最好的自由和开源 3D 内容创建软件的过程。这超越了软件设计和开发，而侧重于作为 Blender 社区一部分的生活。