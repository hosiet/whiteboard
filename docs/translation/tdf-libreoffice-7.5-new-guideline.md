---
tags:
  - 新闻
  - LibreOffice
---

# 入门指南已更新至 LibreOffice 7.5

- 译文信息：
    - 原文： [Getting Started Guide updated to LibreOffice 7.5](https://blog.documentfoundation.org/blog/2023/07/19/getting-started-guide-updated-to-libreoffice-7-5/)
    - 作者：[Olivier Hallot](https://blog.documentfoundation.org/blog/author/ohallot/)
    - 许可证：[CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
    - 译者：暮光的白杨
    - 日期：2023-07-19

---

LibreOffice 文档团队很荣幸地发布最新版的《**入门指南**》，更新了 **LibreOffice 7.5** 的可用功能。

<center>

![点此下载入门指南 7.5](http://books.libreoffice.org/en){ .md-button }

</center>

本书适用于任何希望快速掌握 **LibreOffice 7.5** 的人。书中介绍了 Writer（文字处理）、Calc（电子表格）、Impress（演示文稿）、Draw（矢量绘图）、Math（方程编辑器）和 Base（数据库）。本书由 LibreOffice 社区的志愿者编写。

《**入门指南 7.5**》的更新工作由 **Olivier Hallot** 负责协调，**Peter Schofield**、**Jean Weber**、**flywire** 和 **Nay Catina Dia-Schneebeli** 也做出了宝贵贡献。

![1](./images/2023-07/GS75Team.png)

你可以从[在线网站]下载指南或通过[书架项目]购买实体版书籍。

[在线网站]: https://documentation.libreoffice.org/
[书架项目]: https://books.libreoffice.org/en