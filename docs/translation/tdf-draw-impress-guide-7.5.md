# 为社区提供双份礼物：Impress Guide 7.5 和 Draw Guide 7.5

- 译文信息：
    - 原文： [Double Gift for the Community: Impress Guide 7.5 and Draw Guide 7.5](https://blog.documentfoundation.org/blog/2023/08/09/double-gift-for-the-community-impress-guide-7-5-and-draw-guide-7-5/)
    - 作者： [Olivier Hallot](https://blog.documentfoundation.org/blog/author/ohallot/)
    - 许可证： [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
    - 译者： 暮光的白杨
    - 日期： 2023-08-09

----

社区文档团队很高兴地宣布《**Impress Guide 7.5**》和《**Draw Guide 7.5**》即将发布。这两份指南已更新至最新的 LibreOffice 版本，这是社区为使我们的文档保持敏锐和最新而做出的努力。

<center>

![](./images/2023-08/tdf/75-IMPRESS-AND-DRAW-GRIDE-1024x1024.png){ width=70% }

</center>

这份双重礼物由 **Peter Schofield** 和文档团队成员 **Socks Even** 的宝贵贡献共同为您带来。

《**Impress Guide**》涵盖了 LibreOffice 演示文稿（幻灯片）组件 Impress 的主要功能。您可以创建包含文本、带项目符号和编号的列表、表格、图表、剪贴画和其他对象的幻灯片。Impress 预装了文本样式、幻灯片背景和帮助。它可以打开并保存为 Microsoft PowerPoint 格式，还可以导出为 PDF、HTML 和多种图形格式。本书由 LibreOffice 社区的志愿者编写。

《**Draw Guide**》介绍了 LibreOffice Draw 的主要功能。Draw 是一种矢量图形绘制工具，尽管它也可以对光栅图形（像素）（如照片）执行某些操作。您可以使用 Draw 快速创建各种图形图像。

您可以从[文档网站]以及 [bookshelf project] 下载或购买印刷版。

[文档网站]: https://documentation.libeoffice.org/
[bookshelf project]: https://books.libreoffice.org/en

<span style="font-weight:700;font-size:20px">
祝您阅读愉快！
</span>