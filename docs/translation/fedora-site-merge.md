---
tags:
  - 新闻
  - Fedora
  - 社区动态
---

# Fedora 用户社区变更新闻两则

## Ask Fedora：重大变更即将来临

### 译文信息

- 作者：[Matthew Miller](https://ask.fedoraproject.org/u/mattdm)
- 许可证：不明
    - 类型：新闻邮件  
- 译者：暮光的白杨
- 日期：2022-12-21

----

在 2023 年初的某个时候，该站点（Ask Fedora）[将合并到 Fedora Discussion](https://ask.fedoraproject.org/t/site-merge-into-fedora-discussion-coming-soon-for-real/27622)。 这将使用户和贡献者的交流更加紧密，使用户感受到更加协调统一的 Fedora 社区。我们认为这对于试图决定在哪里发帖的人来说不会再那么混乱（并且当他们最终发错地方的时候，版主也更容易重新移动讨论贴）。

为此，我将在下周在这里做出一些重大改变：

1. 创建一个新的 “Ask in other Language（用其他语言提问）”类别，并将帖子从现有的语言类别移到那里（带有特定语言的标签）；
2. 将一些帖子从 [#start-here](https://ask.fedoraproject.org/c/start-here/96) 移动到 [#english](https://ask.fedoraproject.org/c/english/97) 和它们各自的类别，并将它们设为置顶帖子。合并后，我们可能会将它们放在其他地方……这有望防止它们在下一步行动中丢失；
3. 删除 Start Here、Lounge 和 Staff 类别；
4. 制作条幅帖子解释正在发生的事情以替换此消息。删除公告类别；
5. 查看网站反馈，寻找与合并网站相关的任何内容……删除其他所有内容，最后删除类别。

一旦完成，剩下的一切都应该准备好移植到 Fedora Discussion 作为新的顶级 Ask Fedora 类别的子树。

----

## 好消息！Fedora Discussion 将获得 Enterprise Discourse。

- 原文：[Good news, everyone! Enterprise Discourse is coming to Fedora Discussion](https://discussion.fedoraproject.org/t/good-news-everyone-enterprise-discourse-is-coming-to-fedora-discussion/44738/1)
- 日期：2022-12-11
- 类型：论坛讨论贴

经过一年多的谈判，Red Hat 和为本网站提供支持的 Discourse 论坛软件背后的公司 CDCK 终于达成协议，我们将加入 [Discourse for Enterprise plan 6](https://www.discourse.org/enterprise)。

这会使我们受益颇多。首先也是最基本的，Ask Fedora 已经超过了之前 “Enhanced Business Plan” 的流量限制。Enterprise Discourse 将解决这个问题。它还将使我们能够拥有更多的版主和一些更花哨的插件——包括可能的自动翻译。

这也将促进了原本早该进行的 [Fedora Discussion 与 Ask Fedora 的合并](https://ask.fedoraproject.org/t/considering-a-merge-into-discussion-fedoraproject-org/18941)[^1]计划的实施。

文书工作刚刚完成，敬请期待实际的计划和变化。

🎉🎉🎉🎉🎉🎉🎉🎉🎉

[^1]: 站点合并的详细情况详见此链接。