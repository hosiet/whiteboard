---
tags:
  - 新闻
  - Fedora
---

# 诚邀您参加 Fedora Linux 38 发行派对！

## 译文信息

- 原文：[You’re invited to the Fedora Linux 38 Release Party!](https://fedoramagazine.org/youre-invited-to-the-fedora-linux-38-release-party/)
- 作者：[Justin W. Flory](https://fedoramagazine.org/author/jflory7/)
- 许可证：[CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)
- 译者：暮光的白杨
- 日期：2023-05-31

----

![cover](./images/2023-05/f38-release-party-1024x433.jpg)

请在本周的星期五（6 月 2 日）至周末（6 月 3 日）加入 Fedora 社区，通过虚拟[发行派对][party]庆祝 Fedora Linux 38 的最终版本。你需要在 [Hopin] 上注册并在 6 月 2 日和 3 日加入我们，参加简短的信息会议和社交活动。请务必保存日期，分享注册信息，并与 Fedora 新老友人一起庆祝!

[party]: https://communityblog.fedoraproject.org/tag/release-parties/
[Hopin]: https://hopin.com/events/fedora-linux-38-release-party

[发行派对的日程安排][Schedule]包括有关[重写的 Fedora 通知服务][notifications]的信息会议、[创意自由峰会][cfs]回顾、[Fedora CoreOS][coreos]、来自 [EPEL] 和 [Fedora Docs][doc] 团队的更新以及更多社区活动。最后同样重要的是，我们将在 [Fedora Museum WorkAdventure][museum] 举办活动。感谢我们出色的社区为最新版本的 Fedora Linux 做出的所有贡献。让我们一起狂欢吧！

[Schedule]: https://fedoraproject.org/wiki/Fedora_Linux_38_Release_Party_Schedule
[notifications]: https://notifications.fedoraproject.org/
[cfs]: https://creativefreedomsummit.com/
[coreos]: https://fedoraproject.org/coreos/
[EPEL]: https://docs.fedoraproject.org/en-US/epel/
[doc]: https://docs.fedoraproject.org/en-US/docs/
[museum]: https://play.workadventu.re/@/mindshare-committee/nestwithfedora/fedoramuseum