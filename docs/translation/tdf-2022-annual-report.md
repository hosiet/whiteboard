---
tags:
  - 新闻
  - LibreOffice
---

# 文档基金会 2022 年年度报告

- 译文信息：
    - 原文： [The Document Foundation’s Annual Report 2022](https://blog.documentfoundation.org/blog/2023/08/03/tdf-annual-report-2022/)
    - 作者：[Italo Vignoli](https://blog.documentfoundation.org/blog/author/italovignoli/)
    - 许可证：[CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
    - 译者：暮光的白杨
    - 日期：2023-08-03

----

<center>

![](./images/2023-08/tdf/AR2022-PROOFED-page001-724x1024.png){ width=70% }

</center>

文档基金会年度报告介绍了基金会的活动和项目，特别是有关 [LibreOffice] 和[文档解放项目]的活动和项目。

[LibreOffice]: https://www.libreoffice.org/
[文档解放项目]: https://www.documentliberation.org/

我们已经在博客上发布了 2022 年报告的部分内容，现在完整版的 PDF 格式可以在 TDF 的 Nextcloud 服务器上下载获得；报告有两个不同的版本：[低分辨率]（8.2MB）和[高分辨率]（57.4MB）。年度报告以提交给当局的[德文版本]为基础。

[低分辨率]: https://nextcloud.documentfoundation.org/s/dDDGs2X56jZSKL5
[高分辨率]: https://nextcloud.documentfoundation.org/s/x83j6g2BxPgkkm7
[德文版本]: https://nextcloud.documentfoundation.org/s/GaKDmaLSFcNpiGq

该文件完全由自由与开源软件创建：我们使用 LibreOffice Writer（桌面）编写书面内容，并使用 LibreOffice Writer（在线）进行了协作修改；使用 LibreOffice Calc 创建图表，并用 LibreOffice Draw 准备出版。我们还使用 LibreOffice Draw 编写或修改（提取自以往的 PDF 原件）图纸和表格，并使用 GIMP 编辑图片；版面设计是在现有模板的基础上用 Scribus 创建的。

我们文档基金会非常感谢 2022 年我们项目和社区的所有贡献者——没有你们，这一切都不可能实现！