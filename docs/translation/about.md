---

hide:
  - tags
---

# 关于

!!! warning "版权警告"  

    - 未标注许可证的文章默认以 [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 进行授权；
    - 已注明许可证的文章默认与源文使用的许可证保持一致；
    - ==许可证标注为 **不明** 或 **版权所有** 的文章请勿二次转载。==

!!! note "注意"

    - 此分类一般属于归档文档，如果没有特殊情况，则不再更新。  
    - 所有的文章按发布时间，从新到旧进行以月份分类逆序排序，子类中按时间从旧至新正序排序。  
    - 其他早期文章另见[文档归档库](https://gitlab.com/reuleaux-triangle/Documentation-archive)

此分类主要包含我写的翻译文稿。主要的内容是一些开源组织的新闻稿或者博客，偶尔包含一些我自己觉得有趣的文章。

[openSUSE 中文社区](https://suse.org.cn) 是与本站有诸多关系的新闻站；该站主要为 openSUSE 中文用户提供每周更新的[风滚草](https://get.opensuse.org/tumbleweed/)快照动态、社区新闻、社区活动和重要事项通知等资讯。由于文章数量庞大，所以不再重复录入本站。