# whiteboard

我的个人在线文档站；主要收纳一些开源组织的新闻稿或博客的译文，以及我感兴趣的东西。

- [Cloudflare Page](https://whiteboard-ui8.pages.dev/)

## 如何在本地构建，预览

```
git clone https://gitlab.com/reuleaux-triangle/whiteboard.git && cd whiteboard
```

```
python3 -m venv .venv
```

```
source .venv/bin/activate
```

```
pip install mkdocs mkdocs-material mkdocs-rss-plugin
```

```
mkdocs serve
```

然后你就会在 <http://127.0.0.1:8000/> 看到站点预览。
